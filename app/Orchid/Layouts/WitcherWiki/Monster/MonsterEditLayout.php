<?php

namespace App\Orchid\Layouts\WitcherWiki\Monster;

use App\Models\Providers\WitcherApi\Monster;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;
use Orchid\Support\Color;

class MonsterEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): iterable
    {
        return [
            Input::make('monster.name')
                ->required()
                ->title('Name'),

            Select::make('monster.class')
                ->options(Monster::MONSTER_CLASSES)
                ->required()
                ->title('Select Class'),

            TextArea::make('monster.description')
                ->title('Description')
                ->rows(10),

            Group::make([
                Button::make('Save')
                    ->method('saveMonster')
                    ->icon('save')
                    ->type(Color::SUCCESS()),

                Link::make('Back')
                    ->route('platform.monster.list')
                    ->icon('cancel')
                    ->type(Color::DEFAULT()),

            ])->autoWidth()->alignStart()
        ];
    }
}
