<?php

namespace App\Orchid\Screens\WitcherWiki\Monster;

use App\Models\Providers\WitcherApi\Monster;
use App\Services\Providers\Http\WitcherApi\WitcherApiService;
use Orchid\Screen\Repository;
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;
use Exception;

class MonsterShowScreen extends Screen
{
    private WitcherApiService $witcherService;

    private Monster $monster;

    /**
     * @param WitcherApiService $witcherApiService
     */
    public function __construct(WitcherApiService $witcherApiService)
    {
        $this->witcherService = $witcherApiService;
    }

    /**
     * Query data.
     *
     * @return array
     * @throws Exception
     */
    public function query(string $id): iterable
    {
        $this->fetchMonsterData($id);

        return ['monster' => $this->getMonsterQuery()];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->monster->name;
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::legend('monster', [
                Sight::make('id'),
                Sight::make('name'),
                Sight::make('class'),
                Sight::make('description'),
                Sight::make('createdAt', 'Created'),
                Sight::make('updatedAt', 'Updated'),
            ])->title('Data'),
        ];
    }

    /**
     * @return Repository
     * @throws Exception
     */
    private function getMonsterQuery(): Repository
    {
        return $this->monster->getOrchidRepository();
    }

    /**
     * @param int $id
     * @return void
     * @throws Exception
     */
    private function fetchMonsterData(int $id): void
    {
        $this->monster = $this->witcherService->getMonsterById($id);
    }
}
