<?php

namespace App\Orchid\Screens\WitcherWiki\Monster;

use App\Models\Providers\WitcherApi\Monster;
use App\Orchid\Layouts\WitcherWiki\Monster\MonsterListLayout;
use App\Services\Providers\Http\WitcherApi\WitcherApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Exception;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MonsterListScreen extends Screen
{
    private WitcherApiService $witcherService;

    private Collection $monsters;

    /**
     * @param  $witcherApiService
     */
    public function __construct(WitcherApiService $witcherApiService)
    {
        $this->witcherService = $witcherApiService;
        $this->fetchMonstersData();
    }

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'monsters' => $this->getMonstersQuery()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Monsters';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Create')
                ->modal('createMonsterModal')
                ->method('createMonsterAction')
                ->icon('plus'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout ::modal('createMonsterModal', [
                Layout::rows([
                    Input::make('monster.name')
                        ->required()
                        ->title('Name'),

                    Select::make('monster.class')
                        ->options(Monster::MONSTER_CLASSES)
                        ->required()
                        ->title('Select Class'),

                    TextArea::make('monster.description')
                        ->title('Description')
                        ->rows(10),
                ]),
            ])
                ->title('Create a Monster')
                ->applyButton('Save')
                ->closeButton('Cancel'),
            MonsterListLayout::class
        ];
    }

    /**
     * @param Request $request
     * @return void
     * @throws Exception
     */
    public function createMonsterAction(Request $request): void
    {
        $this->witcherService->createMonster($request->get('monster'));

        Toast::success('Monster created.');
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getMonstersQuery(): array
    {
        return $this->monsters->map(function (Monster $monster) {
            return $monster->getOrchidRepository();
        })->toArray();
    }

    /**
     * @return void
     * @throws Exception
     */
    private function fetchMonstersData(): void
    {
        $this->monsters = $this->witcherService->getMonsters();
    }
}
