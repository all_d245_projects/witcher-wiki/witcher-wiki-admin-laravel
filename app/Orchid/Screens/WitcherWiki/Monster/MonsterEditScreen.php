<?php

namespace App\Orchid\Screens\WitcherWiki\Monster;

use App\Models\Providers\WitcherApi\Monster;
use App\Orchid\Layouts\WitcherWiki\Monster\MonsterEditLayout;
use App\Services\Providers\Http\WitcherApi\WitcherApiService;
use Orchid\Screen\Repository;
use Orchid\Screen\Screen;
use Exception;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Alert;
use Illuminate\Http\RedirectResponse;

class MonsterEditScreen extends Screen
{
    private WitcherApiService $witcherService;

    private Monster $monster;

    /**
     * @param WitcherApiService $witcherApiService
     */
    public function __construct(WitcherApiService $witcherApiService)
    {
        $this->witcherService = $witcherApiService;
    }

    /**
     * Query data.
     *
     * @return array
     * @throws Exception
     */
    public function query(int $monsterId): iterable
    {
        $this->fetchMonsterData($monsterId);

        return ['monster' => $this->getMonsterQuery()];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->monster->name;
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * Views.
     *
     * @return iterable
     */
    public function layout(): iterable
    {
        return [
            MonsterEditLayout::class
        ];
    }

    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function saveMonster(int $id, Request $request): RedirectResponse
    {
        $this->witcherService->updateMonsterById($id, $request->get('monster'));

        Alert::info('You have successfully updated the monster.');

        return redirect()->back();
    }

    /**
     * @return Repository
     * @throws Exception
     */
    private function getMonsterQuery(): Repository
    {
        return $this->monster->getOrchidRepository();
    }

    /**
     * @param int $monsterId
     * @return void
     * @throws Exception
     */
    private function fetchMonsterData(int $monsterId): void
    {
        $this->monster = $this->witcherService->getMonsterById($monsterId);
    }
}
