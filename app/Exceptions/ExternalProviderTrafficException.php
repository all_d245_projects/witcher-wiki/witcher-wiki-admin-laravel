<?php

namespace App\Exceptions;

use Exception;

/**
 * Class ExternalProviderTrafficException
 * @package App\Exception
 */
class ExternalProviderTrafficException extends Exception
{
    /**
     * ExternalProviderTrafficException constructor.
     * @param string $message
     */
    public function __construct(string $message = '')
    {
        parent::__construct($message);
    }
}
