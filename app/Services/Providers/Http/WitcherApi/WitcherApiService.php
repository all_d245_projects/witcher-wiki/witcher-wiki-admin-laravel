<?php

namespace App\Services\Providers\Http\WitcherApi;

use App\Models\Api\ApiResource;
use App\Models\Providers\WitcherApi\Monster;
use App\Services\Providers\Http\AbstractExternalConnectionService;
use Exception;
use Illuminate\Support\Collection;

final class WitcherApiService extends AbstractExternalConnectionService
{
    public const PROVIDER_NAME = 'WITCHER_API';

    private const MONSTER_RESOURCE_NAME = 'monsters';

    public function __construct()
    {
        parent::__construct(resolve(WitcherApiClient::class));
    }

    /**
     * @return Collection<Monster>|null
     * @throws Exception
     */
    public function getMonsters(): ?Collection
    {
        $apiResource = $this->getApiResource();
        $apiResource
            ->addCacheTags(self::MONSTER_RESOURCE_NAME)
            ->setCacheRequest(true)
            ->setDeserializedClass(Monster::class, true)
            ->setName(self::MONSTER_RESOURCE_NAME)
            ->setUrl(self::MONSTER_RESOURCE_NAME);

        $result = $this->getData($this->getHttpClient(), $apiResource);

        return $result->getData();
    }

    /**
     * @param int $monsterId
     * @return Monster|null
     * @throws Exception
     */
    public function getMonsterById(int $monsterId): ?Monster
    {
        $apiResource = $this->getApiResource();
        $apiResource
            ->addCacheTags(self::MONSTER_RESOURCE_NAME)
            ->setCacheRequest(true)
            ->setDeserializedClass(Monster::class)
            ->setName(self::MONSTER_RESOURCE_NAME)
            ->setUrl(sprintf('%s/%s', self::MONSTER_RESOURCE_NAME, $monsterId));

        $result = $this->getData($this->getHttpClient(), $apiResource);

        return $result->getData();
    }

    /**
     * @param int $monsterId
     * @param array $monsterData
     * @return Monster|null
     * @throws Exception
     */
    public function updateMonsterById(int $monsterId, array $monsterData): ?Monster
    {
        $apiResource = $this->getApiResource();
        $apiResource
            ->addCacheTags(self::MONSTER_RESOURCE_NAME)
            ->setRequestOptions(['json' => ['data' => $monsterData]])
            ->setDeserializedClass(Monster::class)
            ->setName(self::MONSTER_RESOURCE_NAME)
            ->setUrl(sprintf('%s/%s', self::MONSTER_RESOURCE_NAME, $monsterId));

        $result = $this->updateData($this->getHttpClient(), $apiResource);

        return $result->getData();
    }

    /**
     * @param array $monsterData
     * @return Monster|null
     * @throws Exception
     */
    public function createMonster(array $monsterData): ?Monster
    {
        $apiResource = $this->getApiResource();
        $apiResource
            ->addCacheTags(self::MONSTER_RESOURCE_NAME)
            ->setRequestOptions(['json' => ['data' => $monsterData]])
            ->setDeserializedClass(Monster::class)
            ->setName(self::MONSTER_RESOURCE_NAME)
            ->setUrl(self::MONSTER_RESOURCE_NAME);

        $result = $this->createData($this->getHttpClient(), $apiResource);

        return $result->getData();
    }

    /**
     * @return ApiResource
     */
    private function getApiResource(): ApiResource
    {
        return new ApiResource(self::PROVIDER_NAME);
    }
}
