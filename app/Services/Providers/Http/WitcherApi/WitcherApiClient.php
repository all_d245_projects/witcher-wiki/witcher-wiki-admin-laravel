<?php

namespace App\Services\Providers\Http\WitcherApi;

use App\Models\Api\ApiResourceInterface;
use App\Services\Providers\Http\AbstractApiClient;
use App\Services\Providers\Http\ExternalConnectionInterface;
use App\Services\Providers\Http\TokenAuthenticationInterface;
use Auth0\SDK\Auth0;
use http\Exception\BadMethodCallException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Client\Response as HttpResponse;
use Exception;
use Auth0\SDK\Exception\NetworkException;
use Auth0\SDK\Exception\ConfigurationException;

class WitcherApiClient extends AbstractApiClient implements ExternalConnectionInterface, TokenAuthenticationInterface
{
    private const AUTH0_TOKEN_CACHE_KEY = 'auth0_access_token';

    private const TIMEOUT = 5;

    public function __construct()
    {
        parent::__construct(config('witcher-admin.witcher-api.api.base_url'));
    }

    /**
     * {@inheritDoc}
     */
    public function sendRequest(string $method, ApiResourceInterface $apiResource): HttpResponse
    {
        $defaultOptions = ['timeout' => $this->getTimeout()];

        $options = array_merge($this->getBaseOptions(), $defaultOptions, $apiResource->getRequestOptions());

        $this->httpClient
            ->withToken($this->getToken())
            ->retry(
                $apiResource->getRetries(),
                ExternalConnectionInterface::RETRY_BACKOFF,
                $apiResource->getRetryCallback()
            );

        return $this->httpClient->send($method, $apiResource->getUrl(), $options);
    }

    /**
     * {@inheritDoc}
     */
    public function getToken(): string
    {
        $token = Cache::get(self::AUTH0_TOKEN_CACHE_KEY);

        if (!$token) {
            $this->fetchToken();

            $auth0 = $this->getAuth0Sdk();

            $ttl = $auth0->getAccessTokenExpiration() - 10;

            Cache::put(self::AUTH0_TOKEN_CACHE_KEY, $auth0->getAccessToken(), $ttl);

            $token = Cache::get(self::AUTH0_TOKEN_CACHE_KEY);
        }

        return $token;
    }

    /**
     * {@inheritDoc}
     */
    public function getTimeout(): int
    {
        return self::TIMEOUT;
    }

    /**
     * @return Auth0
     */
    private function getAuth0Sdk(): Auth0
    {
        return app('auth0')->getSdk();
    }

    /**
     * @throws ConfigurationException
     * @throws NetworkException
     * @throws Exception
     */
    private function fetchToken(): void
    {
        $auth0 = $this->getAuth0Sdk();
        $response = $auth0->authentication()->clientCredentials();

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new Exception('Did not get OK response on Auth0 token retrieval');
        }

        $tokenData = json_decode((string) $response->getBody(), true);

        if (isset($tokenData['access_token']) && isset($tokenData['expires_in'])) {
            $auth0->setAccessToken($tokenData['access_token']);
            $auth0->setAccessTokenExpiration($tokenData['expires_in']);

            if (isset($tokenData['scope'])) {
                $auth0->setAccessTokenScope(explode(' ', $tokenData['scope']));
            }

            return;
        }

        throw new Exception('Invalid Auth0 token data response');
    }
}
