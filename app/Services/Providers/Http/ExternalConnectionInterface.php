<?php

namespace App\Services\Providers\Http;

use App\Models\Api\ApiResourceInterface;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;

interface ExternalConnectionInterface
{
    /**
     * Request made directly without cache
     */
    public const REQUEST_MODE_ORIGIN = 0;

    /**
     * Request made through the cache
     */
    public const REQUEST_MODE_CACHE = 1;

    /**
     * Milliseconds to wait in between attempts
     */
    public const RETRY_BACKOFF = 100;

    /**
     * @param string $method
     * @param ApiResourceInterface $apiResource
     * @return Response
     * @throws RequestException
     */
    public function sendRequest(
        string $method,
        ApiResourceInterface $apiResource
    ): Response;

    /**
     * Returns Guzzle connection timeout in seconds
     *
     * @return integer
     */
    public function getTimeout(): int;
}
