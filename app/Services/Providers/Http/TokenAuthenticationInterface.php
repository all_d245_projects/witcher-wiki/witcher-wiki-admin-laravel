<?php

namespace App\Services\Providers\Http;

interface TokenAuthenticationInterface
{
    /**
     * @return string
     */
    public function getToken(): string;
}
