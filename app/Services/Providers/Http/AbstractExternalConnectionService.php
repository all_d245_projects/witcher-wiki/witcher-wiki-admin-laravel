<?php

namespace App\Services\Providers\Http;

use App\Models\Api\ApiResourceInterface;
use App\Services\CacheService;
use App\Services\DataMapperService;
use App\Models\Api\ApiResponse;
use App\Models\Api\ApiResponseInterface;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Collection;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Client\Response;
use Symfony\Component\HttpFoundation\Response as FoundationResponse;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.LongVariable)
 */
abstract class AbstractExternalConnectionService
{
    /**
     * @var CacheService
     */
    private CacheService $cacheService;

    /**
     * @var DataMapperService
     */
    private DataMapperService $dataMapperService;

    /**
     * @var ExternalConnectionInterface
     */
    private ExternalConnectionInterface $httpClient;

    /**
     * @param ExternalConnectionInterface $httpClient
     */
    public function __construct(ExternalConnectionInterface $httpClient)
    {
        $this->cacheService = resolve(CacheService::class);
        $this->dataMapperService = resolve(DataMapperService::class);
        $this->httpClient = $httpClient;
    }

    /**
     * @return ExternalConnectionInterface
     */
    public function getHttpClient(): ExternalConnectionInterface
    {
        return $this->httpClient;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function getData(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource
    ): ?ApiResponseInterface {
        return ($apiResource->shouldCacheRequest())
            ? $this->handleRequest($connection, $apiResource, 'GET', ExternalConnectionInterface::REQUEST_MODE_CACHE)
            : $this->handleRequest($connection, $apiResource, 'GET');
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function updateData(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb = 'PATCH'
    ): ?ApiResponseInterface {
        $response =  $this->handleRequest($connection, $apiResource, $httpVerb);

        CacheService::invalidateCacheByApiResource($apiResource);

        return $response;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function createData(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb = 'POST'
    ): ?ApiResponseInterface {
        $response =  $this->handleRequest($connection, $apiResource, $httpVerb);

        CacheService::invalidateCacheByApiResource($apiResource);

        return $response;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function deleteData(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb = 'DELETE'
    ): ?ApiResponseInterface {
        $response =  $this->handleRequest($connection, $apiResource, $httpVerb);

        CacheService::invalidateCacheByApiResource($apiResource);

        return $response;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @param int $requestMode
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function handleRequest(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb,
        int $requestMode = ExternalConnectionInterface::REQUEST_MODE_ORIGIN,
    ): ?ApiResponseInterface {
        return match ($requestMode) {
            ExternalConnectionInterface::REQUEST_MODE_ORIGIN =>
                $this->requestViaOrigin($connection, $apiResource, $httpVerb),
            ExternalConnectionInterface::REQUEST_MODE_CACHE =>
                $this->requestViaCache($connection, $apiResource, $httpVerb),
            default => throw new Exception('Invalid request mode ' . $requestMode),
        };
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final protected function requestViaOrigin(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb
    ): ?ApiResponseInterface {
        try {
            $response = $connection->sendRequest($httpVerb, $apiResource);

            $apiResponse = $this->prepareApiResponse($response, $apiResource);
        } catch (RequestException|Exception $exception) {
            $message = 'Request via origin failed.';
            $context = ['class' => __CLASS__, 'function' => __FUNCTION__, 'message' => $exception->getMessage()];

            if ($exception instanceof RequestException) {
                $message = 'Retry ended without success response';
                $context['status'] = $exception->response->status();
                $context['body'] = $exception->response->body();
            }

            Log::error($message, $context);

            abort(FoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $apiResponse;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final protected function requestViaCache(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb
    ): ?ApiResponseInterface {
        try {
            $requestCallable = function () use ($connection, $apiResource, $httpVerb) {
                return $connection->sendRequest($httpVerb, $apiResource);
            };

            /** @var Response $response */
            $response = $this->cacheService->getApiResponseItemFromCache(
                $apiResource,
                $requestCallable
            );

            $apiResponse = $this->prepareApiResponse($response, $apiResource);
        } catch (RequestException|Exception $exception) {
            $message = 'Request via cache failed.';
            $context = ['class' => __CLASS__, 'function' => __FUNCTION__, 'message' => $exception->getMessage()];

            if ($exception instanceof RequestException) {
                $message = 'Retry ended without success response';
                $context['status'] = $exception->response->status();
                $context['body'] = $exception->response->body();
            }

            Log::error($message, $context);

            abort(FoundationResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $apiResponse;
    }

    /**
     * @param Response $response
     * @param ApiResourceInterface $apiResource
     * @return ApiResponseInterface
     * @throws Exception
     */
    private function prepareApiResponse(
        Response $response,
        ApiResourceInterface $apiResource
    ): ApiResponseInterface {
        $apiResponse = new ApiResponse($response);

        if (str_contains($apiResource->getDeserializedClass(), 'Collection')) {
            $apiResponse->setData(new Collection());
        }

        $decodedResponse = $apiResponse->getRawData();


        if (is_array($decodedResponse)) {
            if ($apiResource->getDeserializedClass()) {
                $data = $this->dataMapperService->deserializeAndValidate(
                    json_encode($decodedResponse),
                    $apiResource->getDeserializedClass(),
                    $apiResource->getDeserializationGroups()
                );

                $apiResponse->setData($data);
            }
        }

        return $apiResponse;
    }
}
