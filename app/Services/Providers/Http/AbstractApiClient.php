<?php

namespace App\Services\Providers\Http;

use http\Exception\BadMethodCallException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

abstract class AbstractApiClient
{
    /** @var string  */
    protected string $baseUri;

    /** @var PendingRequest */
    protected PendingRequest $httpClient;

    /**
     * @param string $baseUri
     */
    public function __construct(string $baseUri)
    {
        if (empty($baseUri)) {
            throw new BadMethodCallException(
                sprintf(
                    '[%s][%s] - %s',
                    __CLASS__,
                    __FUNCTION__,
                    'baseUrl cannot be an empty string.'
                )
            );
        }

        $this->baseUri = $baseUri;

        $this->httpClient = Http::baseUrl($this->baseUri);

        if (App::environment('testing')) {
            $this->httpClient->withOptions(['on_stats' => null]);
        }
    }

    /**
     * @return null[]
     */
    protected function getBaseOptions(): array
    {
        $options = [];

        if (App::environment('testing')) {
            $options['on_stats'] = null;
        }

        return $options;
    }
}
