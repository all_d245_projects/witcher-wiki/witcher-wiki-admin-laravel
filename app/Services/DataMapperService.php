<?php

namespace App\Services;

use App\Subscribers\DataMapper\CollectionHandler;
use App\Subscribers\DataMapper\WitcherApiModelSubscriber;
use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Handler\HandlerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;

class DataMapperService
{
    /**
     * @var Serializer
     */
    private Serializer $serializer;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * Serializer cache directory
     *
     * @var string
     */
    private string $cacheDirectory;

    /**
     * @var bool
     */
    private bool $serializerDebug;

    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        AnnotationRegistry::registerLoader('class_exists');
        $this->cacheDirectory = storage_path('app/cache/jms_serializer');
        $this->serializerDebug = config('app.debug');
        $this->serializer = $this->getSerializer();
        $this->validator = $validator;
    }

    /**
     * @param string $data
     * @param string $model
     * @param array $groups
     * @return mixed
     * @throws Exception
     */
    public function deserialize(string $data, string $model, array $groups = [], string $format = 'json')
    {
        try {
            $context = $this->getDeserializationContext($groups);

            $object = $this->serializer->deserialize($data, $model, $format, $context);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $object;
    }

    /**
     * @param string $data
     * @param string $model
     * @param array $groups
     * @return object
     * @throws Exception
     */
    public function deserializeAndValidate(
        string $data,
        string $model,
        array $groups = [],
        string $format = 'json'
    ): object {
        try {
            $context = $this->getDeserializationContext($groups);

            $object = $this->serializer->deserialize($data, $model, $format, $context);

            $errors = $this->validator->validate($object);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        if (count($errors) > 0) {
            $messages = '';

            foreach ($errors as $violation) {
                $messages .= $violation->getPropertyPath() . ':' .  $violation->getMessage() . ',';
            }

            throw new Exception('The model is not valid. ' . $messages . ' Class: ' . $model);
        }

        return $object;
    }

    /**
     * @return Serializer
     */
    private function getSerializer(): Serializer
    {
        return SerializerBuilder::create()
            ->setCacheDir($this->cacheDirectory)
            ->setDebug($this->serializerDebug)
            ->addDefaultHandlers()
            ->configureHandlers(function (HandlerRegistry $registry) {
                $registry->registerSubscribingHandler(new CollectionHandler());
            })
            ->configureListeners(function (EventDispatcher $dispatcher) {
                $dispatcher->addSubscriber(new WitcherApiModelSubscriber());
            })
            ->setSerializationContextFactory(function () {
                return SerializationContext::create()->setSerializeNull(true);
            })
            ->setDeserializationContextFactory(function () {
                return DeserializationContext::create();
            })
            ->build();
    }

    /**
     * @param array $groups
     * @return DeserializationContext|null
     */
    private function getDeserializationContext($groups)
    {
        $context = null;

        if (is_array($groups) && count($groups)) {
            $context = new DeserializationContext();
            $context->setGroups($groups);
        }

        return $context;
    }
}
