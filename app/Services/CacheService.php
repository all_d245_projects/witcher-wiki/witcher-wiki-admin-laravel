<?php

namespace App\Services;

use App\Models\Api\ApiResourceInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Client\Response;
use GuzzleHttp\Psr7\Response as Psr7Response;

use Exception;

class CacheService
{
    public const API_RESPONSE_CACHE_KEY_PREFIX = 'cache_api';

    /**
     * Cache expiration in seconds
     * @var int
     */
    private int $cacheTtl;

    public function __construct()
    {
        $this->cacheTtl = config('witcher-admin.cache_ttl');
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @param callable $requestCallable
     * @return Response
     * @throws Exception
     */
    public function getApiResponseItemFromCache(
        ApiResourceInterface $apiResource,
        callable $requestCallable
    ): Response {
        $key = self::getRequestCacheKeyFromUrl($apiResource);

        $responseData =  Cache::tags($apiResource->getCacheTags())
            ->remember($key, $this->cacheTtl, function () use ($requestCallable) {
                /** @var Response $response */
                $response = $requestCallable();

                return [
                    'statusCode' => $response->status(),
                    'headers' => $response->headers(),
                    'body' => $response->body(),
                    'protocol' => $response->toPsrResponse()->getProtocolVersion(),
                    'reasonPhrase' => $response->reason(),
                ];
            });

        $response = new Response(
            new Psr7Response(
                $responseData['statusCode'],
                $responseData['headers'],
                $responseData['body'],
                $responseData['protocol'],
                $responseData['reasonPhrase']
            )
        );

        if (!$response->successful()) {
            Cache::forget($key);
        }

        return $response;
    }

    /**
     * @param string $url
     * @return string
     */
    public static function getCacheKeyUrlFormat(string $url): string
    {
        $requestString = str_replace('/', '', $url);
        return urlencode($requestString);
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @return string
     */
    public static function getRequestCacheKeyFromUrl(ApiResourceInterface $apiResource): string
    {
        $formattedUrl = self::getCacheKeyUrlFormat($apiResource->getUrl());

        return sprintf(
            '%s_%s_%s',
            self::API_RESPONSE_CACHE_KEY_PREFIX,
            $apiResource->getProvider(),
            $formattedUrl
        );
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @return void
     */
    public static function invalidateCacheByApiResource(ApiResourceInterface $apiResource): void
    {
        Cache::tags($apiResource->getCacheTags())->flush();
    }
}
