<?php

namespace App\Models\Providers\WitcherApi;

use App\Models\Providers\AbstractApiModel;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use DateTime;
use Orchid\Screen\Repository;

class Monster extends AbstractApiModel implements WitcherApiModelInterface
{
    const DATE_TIME_DISPLAY_FORMAT = 'Y-m-d H:i:s';

    const MONSTER_CLASSES = [
        'beasts' => 'Beasts',
        'necrophages' => 'Necrophages'
    ];

    /**
     * @var int|null
     * @Type("integer")
     * @SerializedName("id")
     */
    protected ?int $id;

    /**
     * @var string|null
     * @Type("string")
     * @SerializedName("name")
     */
    protected ?string $name;

    /**
     * @var string|null
     * @Type("string")
     * @SerializedName("class")
     */
    protected ?string $class;

    /**
     * @var string|null
     * @Type("string")
     * @SerializedName("description")
     */
    protected ?string $description;

    /**
     * @var string|null
     * @Type("string")
     * @SerializedName("primaryImage")
     */
    protected ?string $primaryImage;

    /**
     * @var DateTime|null
     * @Type("DateTime<'Y-m-d\TH:i:s.u\Z'>")
     * @SerializedName("createdAt")
     */
    protected ?DateTime $createdAt;

    /**
     * @var DateTime|null
     * @Type("DateTime<'Y-m-d\TH:i:s.u\Z'>")
     * @SerializedName("updatedAt")
     */
    protected ?DateTime $updatedAt;

    public function getOrchidRepository(): Repository
    {
        return new Repository([
            'id' => $this->id,
            'name' =>  $this->name,
            'class' => self::MONSTER_CLASSES[$this->class] ?? $this->class,
            'description' => $this->description,
            'createdAt' => $this->createdAt->format(self::DATE_TIME_DISPLAY_FORMAT),
            'updatedAt' => $this->updatedAt->format(self::DATE_TIME_DISPLAY_FORMAT)
        ]);
    }
}
