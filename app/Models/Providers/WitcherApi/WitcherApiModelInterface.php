<?php

namespace App\Models\Providers\WitcherApi;

interface WitcherApiModelInterface
{
    public const DESERIALIZE_CONTEXT = 'data';
}
