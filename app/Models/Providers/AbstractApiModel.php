<?php

namespace App\Models\Providers;

abstract class AbstractApiModel
{
    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $name): mixed
    {
        return $this->getter($name);
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function getter(string $name): mixed
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_ERROR
        );
    }
}
