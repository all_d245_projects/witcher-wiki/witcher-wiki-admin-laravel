<?php

namespace App\Models\Api;

use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Log;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ApiResource implements ApiResourceInterface
{
    /**
     * @var string
     */
    private string $provider;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $modelClass;

    /**
     * @var ?string
     */
    private ?string $deserializedClass = null;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var int
     */
    private int $retries = 3;

    /**
     * @var boolean
     */
    private bool $cacheRequests = false;

    /**
     * @var callable|null
     */
    private $retryCallback = null;

    /**
     * @var array
     */
    private array $cacheTags = [];

    /**
     * @var array
     */
    private array $requestOptions = [];

    /**
     * @var array
     */
    private array $header = [];

    /**
     * @var array
     */
    private array $retryStatusExempt = [];

    /**
     * @var array
     */
    private array $deserializationGroups = [];

    /**
     * @param string $provider
     */
    public function __construct(string $provider)
    {
        $this->provider = $provider;
    }

    /**
     * {@inheritDoc}
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ApiResource
     */
    public function setName(string $name): ApiResource
    {
        $this->name = $name;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    /**
     * @param string $class
     * @return ApiResource
     */
    public function setModelClass(string $class): ApiResource
    {
        $this->modelClass = $class;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDeserializedClass(): ?string
    {
        return $this->deserializedClass;
    }

    /**
     * @param string $class
     * @param bool $isCollection
     * @return ApiResource
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function setDeserializedClass(string $class, bool $isCollection = false): ApiResource
    {
        ($isCollection)
            ? $this->deserializedClass = sprintf('Collection<%s>', $class)
            : $this->deserializedClass = $class;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDeserializationGroups(): array
    {
        return $this->deserializationGroups;
    }

    /**
     * @param array $groups
     * @return $this
     */
    public function setDeserializationGroups(array $groups): ApiResource
    {
        $this->deserializationGroups = $groups;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ApiResource
     */
    public function setUrl(string $url): ApiResource
    {
        $this->url = $url;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHeaders(): array
    {
        return $this->header;
    }

    /**
     * @param array $header
     * @return ApiResource
     */
    public function setHeaders(array $header): ApiResource
    {
        $this->header = $header;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getFormattedResponse(array $response)
    {
        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function getRetries(): int
    {
        return $this->retries;
    }

    /**
     * @param integer $retries
     * @return ApiResource
     */
    public function setRetries(int $retries): ApiResource
    {
        $this->retries = $retries;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldCacheRequest(): bool
    {
        return $this->cacheRequests;
    }

    /**
     * @param boolean $value
     * @return ApiResource
     */
    public function setCacheRequest(bool $value): ApiResource
    {
        $this->cacheRequests = $value;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRequestOptions(): array
    {
        return $this->requestOptions;
    }

    /**
     * @param array $requestOptions
     * @return ApiResource
     */
    public function setRequestOptions(array $requestOptions): ApiResource
    {
        $this->requestOptions = $requestOptions;
        return $this;
    }

    /**
     * @param array $retryStatusExempt
     * @return ApiResource
     */
    public function setRetryStatusExempt(array $retryStatusExempt): ApiResource
    {
        $this->retryStatusExempt = $retryStatusExempt;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRetryStatusExempt(): array
    {
        return (empty($this->retryStatusExempt))
            ? self::RETRY_STATUS_EXEMPT
            : $this->retryStatusExempt;
    }

    /**
     * @param array $cacheTags
     * @return ApiResource
     */
    public function setCacheTags(array $cacheTags): ApiResource
    {
        $this->cacheTags = $cacheTags;
        return $this;
    }

    /**
     * @param string ...$tags
     * @return ApiResource
     */
    public function addCacheTags(string ...$tags): ApiResource
    {
        foreach ($tags as $tag) {
            if (!in_array($tag, $this->cacheTags)) {
                $this->cacheTags[] = sprintf('%s:%s', $this->provider, $tag);
            }
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getCacheTags(): array
    {
        return $this->cacheTags;
    }

    /**
     * @param callable $callback
     * @return ApiResource
     */
    public function setRetryCallback(callable $callback): ApiResource
    {
        $this->retryCallback = $callback;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRetryCallback(): callable
    {
        return $this->retryCallback
            ?: function ($exception) {
                Log::info('HTTP Attempt failed', ['message' => $exception->getMessage()]);

                return match (true) {
                    $exception instanceof RequestException &&
                    in_array($exception->response->status(), ApiResourceInterface::RETRY_STATUS_EXEMPT) => false,
                    default => true,
                };
            };
    }
}
