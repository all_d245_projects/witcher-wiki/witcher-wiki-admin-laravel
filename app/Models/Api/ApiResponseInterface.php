<?php

namespace App\Models\Api;

use Illuminate\Http\Client\Response;

interface ApiResponseInterface
{
    /**
     * Returns deserialized data
     *
     * @return mixed
     */
    public function getData();

    /**
     * Set deserialized data
     *
     * @param mixed $data
     * @return ApiResponseInterface
     */
    public function setData(mixed $data): ApiResponseInterface;

    /**
     * Returns the Laravel HTTP Response
     *
     * @return Response
     */
    public function getResponse(): Response;

    /**
     * Returns json encoded body response if possible or response body string
     *
     * @return array|string
     */
    public function getRawData(): array|string;

    /**
     * Returns the headers of the response
     *
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Returns response status code
     *
     * @return integer
     */
    public function getStatusCode(): int;
}
