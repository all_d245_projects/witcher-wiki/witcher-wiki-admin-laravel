<?php

namespace App\Models\Api;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Collection;

class ApiResponse implements ApiResponseInterface
{
    /**
     * Deserialized data
     * @var Collection|object|null
     */
    private mixed $data = null;

    /**
     * @var Response
     */
    private Response $response;

    /**
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Collection|object|null
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    /**
     * @param Collection|object $data
     * {@inheritDoc}
     */
    public function setData($data): ApiResponse
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * {@inheritDoc}
     */
    public function getRawData(): array|string
    {
        $body = $this->response->body();

        $rawData = json_decode($body, true);

        return (is_array($rawData))
            ? $rawData
            : $body;
    }

    /**
     * {@inheritDoc}
     */
    public function getHeaders(): array
    {
        return $this->response->headers();
    }

    /**
     * {@inheritDoc}
     */
    public function getStatusCode(): int
    {
        return $this->response->status();
    }
}
