<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\Dashboard;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard)
    {
        $monsterPermissions = ItemPermission::group('monsters')
            ->addPermission('monster:read', 'View monsters')
            ->addPermission('monster:create', 'Create monsters')
            ->addPermission('monster:update', 'Edit monsters')
            ->addPermission('monster:delete', 'Delete monsters');

        $dashboard->registerPermissions($monsterPermissions);
    }
}
