<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Validation;
use App\Services\DataMapperService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ValidatorInterface::class, function () {
            return Validation::createValidator();
        });

        $this->app->singleton(DataMapperService::class, function ($app) {
            return new DataMapperService($app->make(ValidatorInterface::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
