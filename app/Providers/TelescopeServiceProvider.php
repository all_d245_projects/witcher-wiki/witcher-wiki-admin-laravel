<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Telescope::night();
        $this->hideSensitiveRequestDetails();
    }

    /**
     * Prevent sensitive request details from being logged by Telescope.
     *
     * @return void
     */
    protected function hideSensitiveRequestDetails()
    {
        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    /**
     * Configure the Telescope authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();
        Telescope::auth(function ($request) {
            return $this->isEnvironmentValidForTelescope() ||
                Gate::check('viewTelescope', [$request->user()]);
        });
    }


    /**
     * @return boolean
     */
    private static function isEnvironmentValidForTelescope(): bool
    {
        $app = app();
        return $app->environment('local') || $app->environment('development');
    }
}
