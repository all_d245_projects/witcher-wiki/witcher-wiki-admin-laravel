<?php

namespace App\Subscribers\DataMapper;

use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\PreDeserializeEvent;
use ReflectionClassConstant;
use Exception;

final class WitcherApiModelSubscriber implements EventSubscriberInterface
{
    /**
     * @return string[][]
     */
    public static function getSubscribedEvents(): array
    {
        // todo: Make this subscriber more exclusive to WitcherApiModelInterface
        return [
            [
                'event' => 'serializer.pre_deserialize',
                'method' => 'onPreDeserialize',
            ],
        ];
    }

    /**
     * @param PreDeserializeEvent $event
     */
    public function onPreDeserialize(PreDeserializeEvent $event): void
    {
        try {
            $fullyQualifiedClass = $this->getFullyQualifiedClass($event);

            if ($fullyQualifiedClass) {
                $constant = new ReflectionClassConstant($fullyQualifiedClass, 'DESERIALIZE_CONTEXT');

                $data = $event->getData();

                $contextData = $this->getDeserializationData($data, $constant->getValue());

                $event->setData($contextData);
            }
        } catch (Exception $exception) {
            // could not get data with deserialization context...continue.
        }
    }

    /**
     * @param array $data
     * @param string $context
     * @return array
     * @throws Exception
     */
    private function getDeserializationData(array $data, string $context): array
    {
        $keys = explode('.', $context);

        $contextData = $data;

        foreach ($keys as $key) {
            if (!isset($contextData[$key])) {
                throw new Exception('Could not build data access path.');
            }

            $contextData = $contextData[$key];
        }

        return $contextData;
    }

    /**
     * @param PreDeserializeEvent $event
     * @return string|null
     */
    private function getFullyQualifiedClass(PreDeserializeEvent $event): ?string
    {
        $type = $event->getType();

        return (empty($type['params']))
            ? $event->getType()['name'] ?? null
            : $event->getType()['params'][0]['name'] ?? null;
    }
}
