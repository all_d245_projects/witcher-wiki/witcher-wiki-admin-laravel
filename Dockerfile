####################################################
# Witcher Wiki Admin - Laravel
# PHP 8.1 / FPM-NGINX image
####################################################
FROM registry.gitlab.com/all_d245_projects/docker/docker-php:8.1-fpm-nginx-latest

LABEL maintainer="Aldrich Saltson (@all_d245)"

COPY --chown=${PUID}:${PGID} . /var/www/html/

ENTRYPOINT ["/init"]
