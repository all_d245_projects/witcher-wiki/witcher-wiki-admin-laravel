<?php

namespace Tests\TestCases;

use allejo\VCR\VCRCleaner;

class VcrTestCase extends LaravelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param array $options
     * @return void
     */
    public function enableWitcherApiVcrCleaner(array $options = []): void
    {
        $options['request']['ignoreHeaders'][] = 'Authorization';
        $options['request']['bodyScrubbers'][] = function ($body) {
            return preg_replace('/client_secret=.*&/', '####CLIENT-SECRET-REDACTED####', $body);
        };

        VCRCleaner::enable($options);
    }
}
