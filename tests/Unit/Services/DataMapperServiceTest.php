<?php // phpcs:ignore PSR1.Files.SideEffects.FoundWithSymbols

use App\Models\Providers\WitcherApi\Monster;
use App\Services\DataMapperService;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

beforeEach(function () {
    $this->validatorMock = Mockery::mock(ValidatorInterface::class);
    $this->constraintViolationMock = Mockery::mock(ConstraintViolationListInterface::class);
});

test('deserialize single model ok', function () {
    $dataMapperService = getDataMapperService();

    $dataString = getDataString();

    /** @var Monster $result */
    $result = $dataMapperService->deserialize($dataString, Monster::class);

    expect($result)->toBeInstanceof(Monster::class);
    expect($result->name)->toEqual('Bear');
    expect($result->class)->toEqual('beasts');
    expect($result->description)->toEqual('description');
})->group('data-mapper-server', 'ok');

test('deserializeAndValidate single model ok', function () {
    setDefaultMocks();
    $dataMapperService = getDataMapperService();

    $dataString = getDataString();

    /** @var Monster $result */
    $result = $dataMapperService->deserializeAndValidate($dataString, Monster::class);

    expect($result)->toBeInstanceof(Monster::class);
    expect($result->name)->toEqual('Bear');
    expect($result->class)->toEqual('beasts');
    expect($result->description)->toEqual('description');
})->group('data-mapper-server', 'ok')->skip();

/**
 * @return string
 */
function getDataString(): string
{
    return json_encode(getValidMonster());
}

/**
 * @return DataMapperService
 */
function getDataMapperService(): DataMapperService
{
    return new DataMapperService(test()->validatorMock);
}

function setDefaultMocks(): void
{
    test()->validatorMock
        ->shouldReceive('validate')
        ->zeroOrMoreTimes()
        ->andReturn(test()->constraintViolationMock);
}
