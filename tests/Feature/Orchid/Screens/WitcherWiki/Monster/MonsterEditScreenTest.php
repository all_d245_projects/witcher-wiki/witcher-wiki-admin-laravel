<?php

namespace Tests\Feature\Orchid\Screens\WitcherWiki\Monster;

use App\Models\Providers\WitcherApi\Monster;
use App\Services\Providers\WitcherApi\WitcherApiService;
use Illuminate\Support\Collection;
use Tests\TestCases\VcrTestCase;
use Orchid\Support\Testing\ScreenTesting;

class MonsterEditScreenTest extends VcrTestCase
{
    use ScreenTesting;

    public function setUp(): void
    {
        $this->enableWitcherApiVcrCleaner();
        parent::setUp();
    }

    /**
     * @vcr monster_list_screen_test_OK_VCR.yaml
     * @group witcher-api
     * @group ok
     * @group dev
     */
    public function test_monster_list_screen()
    {
        $this->markTestSkipped();
        $screen = $this->screen('platform.monster.list');

        $screen->display()
            ->assertSee('Monsters');
    }
}
