<?php

namespace Tests\Feature\Orchid\Screens\WitcherWiki\Monster;

use App\Models\Eloquent\User;
use Tests\TestCases\VcrTestCase;
use Orchid\Support\Testing\ScreenTesting;

class MonsterListScreenTest extends VcrTestCase
{
    use ScreenTesting;

    public function setUp(): void
    {
        $this->enableWitcherApiVcrCleaner();
        parent::setUp();
    }

    /**
     * @vcr monster_list_screen_test_OK_VCR.yaml
     * @group witcher-api
     * @group ok
     * @group dev
     */
    public function test_monster_list_screen()
    {
        $this->markTestSkipped();
        $user = User::factory()->create();
        $screen = $this->screen('platform.monster.list')->actingAs($user);

        $screen->display()
            ->assertSee('Monsters');
    }
}
