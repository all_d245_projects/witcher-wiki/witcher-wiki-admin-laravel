<?php

namespace Tests\Feature\Services\Providers\WitcherApi;

use App\Models\Providers\WitcherApi\Monster;
use App\Services\Providers\Http\WitcherApi\WitcherApiService;
use Illuminate\Support\Collection;
use Tests\TestCases\VcrTestCase;

class WitcherApiServiceTest extends VcrTestCase
{
    /**
     * @var WitcherApiService|mixed
     */
    private WitcherApiService $witcherApiService;

    public function setUp(): void
    {
        $this->enableWitcherApiVcrCleaner();
        parent::setUp();
        $this->witcherApiService = $this->app->make(WitcherApiService::class);
    }

    /**
     * @vcr witcher_api_service_test_OK_VCR.yaml
     * @group witcher-api
     * @group ok
     */
    public function test_get_monsters_returns_monsters_collection()
    {
        $monsters = $this->witcherApiService->getMonsters();
        $this->assertInstanceOf(Collection::class, $monsters);
    }

    /**
     * @vcr witcher_api_service_test_OK_VCR.yaml
     * @group witcher-api
     * @group ok
     */
    public function test_get_monster_by_id_returns_monsters_model()
    {
        $monster = $this->witcherApiService->getMonsterById(1);
        $this->assertInstanceOf(Monster::class, $monster);
        $this->assertEquals('Bear', $monster->name);
        $this->assertEquals('beasts', $monster->class);
    }
}
