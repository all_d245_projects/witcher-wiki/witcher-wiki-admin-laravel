<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Witcher Wiki Admin - Laravel

### Description
Witcher Wiki Admin built with the Laravel framework.

### Installation
1. Download and install Docker with Docker Compose and Npm.
2. Clone the project.
3. Execute the script below to run a docker container containing PHP and Composer to install the application's PHP dependencies with the appropriate PHP version (PHP 8.0):
    ```bash
    docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
    ```
4. Install npm dependencies
    ```bash
    ./vendor/bin/sail npm install
    ```
5. Compile assets
    ```bash
    ./vendor/bin/sail npm run dev
    ```
6. install husky
    ```bash
   ./vendor/bin/sail npm run husky-install
   ```
7. Make a copy of the .env.example:
``` bash
cp .env.example .env
```
8. Generate the application key.
```bash
./vendor/bin/sail artisan key:generate
```
9. Run migrations:
```bash
./vendor/bin/sail artisan migrate:fresh --seed
```
10. Modify the forward ports defined in the `docker-compose.yml` file to available ports on your local machine.
1. DOCKER_APP_PORT           | default 80
2. DOCKER_FORWARD_REDIS_PORT | default 6379
3. DOCKER_FORWARD_MYSQL_PORT | default 3360

### Running the app
The project startup is managed by a bash script wrapper.

Start project services via docker (ensure docker engine is running)
```bash
./vendor/bin/sail up -d
```

After the containers startup, you can access the application at `localhost:<DOCKER_APP_PORT>`

### Stopping the app
```bash
./vendor/bin/sail stop
```

### Running commands
```bash
# artisan commands
./vendor/bin/sail artisan <command>
```

```bash
# php commands
./vendor/bin/sail php <command>
```

```bash
# composer commands
./vendor/bin/sail composer <command>
```

```bash
# node / npm commands
./vendor/bin/sail npm run <commands>
```

```bash
# container cli
./vendor/bin/sail shell

# root shell
./vendor/bin/sail root-shell
```

```bash
# laravel tinker
./vendor/bin/sail tinker
```

```bash
# rebuild image with no cache
./vendor/bin/sail build --no-cache
```

### Git Commit
The project adheres to the commit message [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
The structure used is
```
<type>: <issue title> [<issue-code>]
Closes <issue code>
```

An example commit would be:
```
feat: add http client for provider [#256]
Closes #256
```

The supported `types` for this project are defined in `commitlint.config.js`.

This commit structure is not enforced locally, however there is a pipeline test to ensure this convention is followed.
At the moment the pipeline test does not enforce the existence and position of `[<issue-code>]` in the commit message header, or the existence of `Closes <issue-code>` in the commit message body. These two elements should still be included.

### Tests
To execute PHP unit tests, make a local copy of the file `.env.testing.example`.
```bash
cp .env.testing.example .env.testing
```

PHP unit tests can be run via Laravel sail
```bash
./vendor/bin/sail test
```

All tests (PHP Mess detector, PHP Code Sniffer, PHPSTAN, PHPUNIT) can be run via the bash script `bin/tests`:
```bash
./vendor/bin/sail shell bin/tests
```

### Migrations
Execute database migrations by running the command:
```bash
./vendor/bin/sail artisan migrate
```

Drop all tables and run migration with seeds by running the command:
```bash
./vendor/bin/sail artisan migrate:fresh --seed
```
### Pipeline
Every merge request to the branch development and main will initiate a detached pipeline.
The pipeline has the following jobs:

- Merge Request
    - install-dependencies-npm: Installs the npm dependencies and runs laravel mix in dev mode
    - install-dependencies-composer: Installs the composer dependencies
    - test-prepare: makes a copy of the .env.testing to be used in the testing jobs
    - app-tests-lint-commit: runs commit lint tests
    - app-tests-phpunit: runs phpunit tests (unit/feature) with reports
    - app-tests-codesniffer: runs phpcs tests
    - app-tests-messdetector: runs phpmd tests
    - app-tests-phpstan: runs phpstan tests

### Authentication/Authorization
TBC
