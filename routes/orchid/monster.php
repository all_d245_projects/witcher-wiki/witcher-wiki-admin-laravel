<?php

use App\Orchid\Screens\WitcherWiki\Monster\MonsterEditScreen;
use App\Orchid\Screens\WitcherWiki\Monster\MonsterListScreen;
use App\Orchid\Screens\WitcherWiki\Monster\MonsterShowScreen;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Monster Routes
|--------------------------------------------------------------------------
*/

$baseRoute = 'monsters';

Route::screen($baseRoute, MonsterListScreen::class)
    ->name('platform.monster.list')
    ->middleware('access:monster:read');

Route::screen($baseRoute . '/{monster}/edit', MonsterEditScreen::class)
    ->name('platform.monster.edit')
    ->middleware('access:monster:update');

Route::screen($baseRoute . '/{monster}/show', MonsterShowScreen::class)
    ->name('platform.monster.show')
    ->middleware('access:monster:read');
